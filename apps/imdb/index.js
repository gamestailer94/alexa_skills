'use strict';

const imdb = require('imdb-api');
const alexa = require('alexa-app');
const q = require('q');
module.change_code = 1;

let app = new alexa.app('imdb');

app.launch(function (req, res) {
  res.say('Imdb Skill, Movie or Series name ?');
  res.shouldEndSession(false);
  res.send()
});

app.intent('queryIntent', {
  "utterances": [
    "rating{| from|for} {-|MOVIE}",
    "rating{| from|for} {-|SERIES}"
  ],
  "slots": {
    "MOVIE": "AMAZON.Movie",
    "SERIES": "AMAZON.TVSeries"
  },
}, function (req, res) {
    let movie = req.slot('MOVIE');
    let series = req.slot('SERIES');
    let finalName = movie || series;
    if(!finalName){
      res.say('No Name given!');
      res.send();
    }else {
      let imdbReq = imdb.getReq({name: finalName});
      return imdbReq.then(function (resp) {
        let rating = resp.rating.replace('.',',');
        res.say('The Rating from ' + finalName + ' is: ' + rating + ' from 10');
        res.send();
      }, function () {
        res.say('Cant get rating for that name!');
        res.send();
      });
    }
});

module.exports = app;