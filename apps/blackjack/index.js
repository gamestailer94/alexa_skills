'use strict';
const Random = require('random-js');
const random = new Random(Random.engines.browserCrypto);
const alexa = require('alexa-app');
module.change_code = 1;

let app = new alexa.app('blackjack');

const cards = {
    0: 'Ass',
    1: 2,
    2: 3,
    3: 4,
    4: 5,
    5: 6,
    6: 7,
    7: 8,
    8: 9,
    9: 10,
    10: 'Bube',
    11: 'Dame',
    12: 'König'
};

const colors = {
    0: 'Herz',
    1: 'Karo',
    2: 'Pik',
    3: 'Kreuz'
};

const _played = {
    0: {},
    1: {},
    2: {},
    3: {},
    "value": 0
};

let played = {};

// Alexa Calls
app.launch(function (req, res) {
    let session = req.getSession();
    session.set('hand', session.get('hand') || _played);
    session.set('played', session.get('played') || _played);
    session.set('dealerHand', session.get('dealerHand') || _played);
    session.set('running', session.get('running') || false);

    res.say('Willkommen zu Blackjack.');
    if (session.get('running')) {
        res.say('Es läuft bereits ein Spiel, umd deine Hand zu erfahren sage <break></break>Hand.');
    } else {
        res.say('Um ein Spiel zu starten sage <break></break>start<break></break> für die Regeln sage <break></break>Hilfe.');
    }
    res.shouldEndSession(false);
    res.send()
});
app.intent('startIntent', {
    "utterances": [
        "start",
        "neues Spiel",
        "spiel starten"
    ]
}, startGame);
app.intent('hitIntent', {
    "utterances": [
        'hit',
        'noch{ | eine}{ | karte}{ | mehr}',
        'weiter{|e} {Karte|}'
    ]
}, hitCard);
app.intent('foldIntent', {
    "utterances": [
        "fold",
        "keine{ | karte}{ | mehr}",
        "das reicht {|mir}"
    ]
}, foldCard);
app.intent('handIntent', {
    "utterances": [
        "hand",
        "was habe ich {auf|in} der hand",
        "meine {karten|hand}"
    ]
}, getHand);
app.intent('AMAZON.StopIntent', function (req, res) {
    res.say('Bis bald');
    res.shouldEndSession(true);
});
app.intent('AMAZON.CancelIntent', function (req, res) {
  res.say('Bis bald');
  res.shouldEndSession(true);
});
app.intent('AMAZON.HelpIntent', {
    "utterances": [
        "regeln"
    ]
}, function (req, res) {
    res.say('Bei Blackjack geht es darum so nah wie möglich, aber nicht über, 21 Punkte zu kommen.');
    res.say('Jeder Spieler erhält Karten mit einem bestimmten Wert. 2-10 haben den ensprechenden Wert, Bildkarten (Bube, Dame, König) sind jeweils 10 Wert,');
    res.say('Ein Ass ist entweder 1 oder 11 Wert, je nachdem was günstiger für den Spieler ist.');
    res.say('Um eine weitere Karte zu bekommen sage <break></break>Hit<break></break> um keine Karten zu bekommen sage <break></break>Fold.');
    res.say('Zum starten sage <break></break>start.');
    res.shouldEndSession(false);
});
app.error = function(exception, request, response) {
    response.say("Ein Fehler ist aufgetreten.");
    response.say(exception.message);
};

function getHand(req, res) {
    let session = req.getSession();
    if (!session.get('running')) {
        notRunning(req, res)
    } else {
        let hand = session.get('hand');
        let handCards = readCards(hand);
        if (handCards === '') {
            res.say('Deine Hand ist leer.');
            res.shouldEndSession(false)
        } else {
            res.say("Du hast " + handCards + ' auf der Hand.');
            res.say("Das sind " + hand.value + ' Punkte.');
            res.shouldEndSession(false);
        }
    }
}

function startGame(req, res) {
    let session = req.getSession();
    session.set('hand', _played);
    session.set('played', _played);
    session.set('dealerHand', _played);
    session.set('asses', {"hand": _played, "dealerHand": _played});
    session.set('running', true);
    let card = playCard(session, false);
    playCard(session, true);
    res.say('Ich erhalte eine verdeckte Karte.');
    res.say('Du erhälst eine Karte: ' + getCardName(card[0], card[1]));
    res.shouldEndSession(false);
}

function hitCard(req, res) {
    let session = req.getSession();
    if (!session.get('running')) {
        notRunning(req, res)
    } else {
        let card = playCard(session, false);
        res.say('Du erhälst eine Karte: ' + getCardName(card[0], card[1]) + '<break></break>');
        if (session.get('hand').value < 22) {
            res.say('Du hast aktuell ' + session.get('hand').value + ' Punkte.');
        } else {
            res.say('Du hast verloren, da du über 21 Punkte gekommen bist.');
            res.say('Wenn du noch eine Runde spielen willst sage <break></break>start');
            session.set('running', false);
        }
        res.shouldEndSession(false);
    }
}

function foldCard(req, res) {
    let session = req.getSession();
    if (!session.get('running')) {
        notRunning(req, res)
    } else {
        played = session.get('played');
        res.say('Du willst keine Karten mehr.');
        while (shouldPicCard(session)) {
            let card = playCard(session, true);
            res.say('Ich ziehe eine Karte: ' + getCardName(card[0], card[1]) + '<break></break>');
        }
        let value = session.get('hand').value;
        let dealerValue = session.get('dealerHand').value;
        if (dealerValue > 21) {
            res.say('Ich habe über 21 Punkte, du hast gewonnen.')
        } else {
            if (value > dealerValue) {
                res.say('Du hast gewonnen. Mit ' + value + ' zu ' + dealerValue + ' Punkten.');
            } else if (value === dealerValue) {
                res.say('Patt. ' + value + ' zu ' + dealerValue + ' Punkte.');
            } else if (value < dealerValue) {
                res.say('Ich habe gewonnen, Mit ' + dealerValue + ' zu ' + value + ' Punkten.')
            }
        }
        res.say('Wenn du noch eine Runde spielen willst sage <break></break>start');
        session.set('running', false);
        res.shouldEndSession(false);
    }
}

function notRunning(req, res) {
    res.say('Du bist nicht in einem Spiel, sage <break></break>Start<break></break> um eines zu beginnen.');
    res.shouldEndSession(false);
}

// helper functions

function shouldPicCard(session) {
    let value = session.get('dealerHand').value;
    return value <= 15;

}

function playCard(session, isDealer) {
    let played = session.get('played');
    let card = getRandomCard(played);
    let handVar = session.get('hand');
    if (isDealer) {
        handVar = session.get('dealerHand');
    }
    played[card[0]][card[1]] = true;
    handVar[card[0]][card[1]] = true;
    handVar.value = parseInt(handVar.value) + parseInt(getCardValue(card[1], handVar.value));
    if (isDealer) {
        session.set('dealerHand', handVar);
    } else {
        session.set('hand', handVar);
    }
    session.set('played', played);
    return card;
}

function getCardValue(card, handValue) {
    let value = 0;
    if (card >= 10) {
        value = 10;
    } else if (card === 0) {
        if (handValue > 11) {
            value = 1;
        } else {
            value = 11;
        }
    } else {
        value = cards[card];
    }
    return value;
}

function readCards(hand) {
    let cardNames = [];
    for (let i = 0; i < 4; i++) {
        let curColor = hand[i];
        for (let a = 0; a < 15; a++) {
            let curCard = curColor[a] || false;
            if (curCard) {
                cardNames.push(getCardName(i, a));
            }
        }
    }
    return cardNames.join(', ');
}

function getRandomCard(played) {
    let card = random.integer(0, 12);
    let color = random.integer(0, 3);
    if (played[color][card] === undefined || played[color][card] === null) {
        return [color, card]
    } else {
        return getRandomCard(played)
    }
}

function getCardName(color, card) {
    let NameColor = colors[color];
    let NameCard = cards[card];
    return NameColor + ' ' + NameCard;
}

module.exports = app;