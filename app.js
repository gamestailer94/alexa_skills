'use strict';
let AlexaAppServer = require('alexa-app-server');
let debugApp = false;
if (process.env.NODE_ENV === 'development') {
    debugApp = true;
    console.log('DEBUG MODE !!!');
}
let server = new AlexaAppServer({
    server_root: __dirname,     // Path to root
    app_dir: "apps",       // Service root
    port: 3000,
    debug: debugApp
});

server.start();